PORT=8095
#NETWORK_EXTERNAL=--network=caddynet

# Stop previously running container.
docker stop dicu-model-dialogpt
# Note, requires nvidia runtime support - refer to README.md!
docker run --rm -d \
    --runtime=nvidia \
    -p $PORT:8080 \
    $NETWORK_EXTERNAL \
    --name dicu-model-dialogpt \
    dicu-model-dialogpt
