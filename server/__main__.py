from sanic.response import json
from sanic import Sanic
import ujson as js
import logging
import time

from .model import DialoGPTConfig, DialoGPT


app = Sanic(name="dicu-dialogpt-ai")
config = DialoGPTConfig()
model = DialoGPT(config)


@app.route("/api/get_response", methods=["POST"])
async def api_get_response(request):
    data = js.loads(request.json) if type(request.json) == str else request.json

    if not (user := data.get("user_id")):
        return json({"status": 500, "message": "You must provide 'user_id' with data."}, status=500)

    prompt = data.get("prompt", "").strip(" \n\r\t")
    # This is allowed intentionally, but lets still log it for now.
    logging.warning("Empty prompt value received over API. User '%s' ...", user)

    # Set/update history, if history is provided.
    if history := data.get("history", []):
        model.set_initial_history(user, history[0])
        for message in history[1:]:  model.update_chat_history(user, message)

    # Generate response.
    resp = (await model.aget_response(user, prompt, data.get("input_ids_extern"))).strip(" \n\r\t")

    # Save history.
    model.update_chat_history(user, prompt)
    model.update_chat_history(user, resp)

    # Update history object:
    history.append(prompt)
    history.append(resp)

    return json({"timestamp": time.monotonic(), "text": resp, "history": history})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
