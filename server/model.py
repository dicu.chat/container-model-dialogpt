from transformers import AutoModelForCausalLM, AutoTokenizer
from collections import defaultdict
import numpy as np
import asyncio
import logging
import torch


class DialoGPTConfig:
    def __init__(
        self,
        name: str                 = "Dialo",
        mname: str                = "microsoft/DialoGPT-large",
        persona: str              = "",
        max_len_history: int      = 128,
        # More diaglogpt specific vars:
        do_sample: bool           = True,
        top_p: float              = .85,
        top_k: int                = 100,
        temperature: float        = .75,
        repetition_penalty: float = 1.05,
        # Device type (GPU/CPU) for the model to use:
        device = None,
    ):
        self.name = name
        self.mname = mname
        self.persona = persona
        self.max_len_history = max_len_history
        # More diaglogpt specific vars:
        self.do_sample = do_sample
        self.top_p = top_p
        self.top_k = top_k
        self.temperature = temperature
        self.repetition_penalty = repetition_penalty

        # self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
        self.device: str = device or "cpu"
        logging.info("CUDA available: %s. Using device: %s", torch.cuda.is_available(), self.device)


class DialoGPT:
    """
    Dialogpt model from huggingface with functions for chat
    """

    def __init__(self, config: DialoGPTConfig):
        logging.info("Loading %s model %s ...", config.name, config.mname)

        self.config = config

        self.model = AutoModelForCausalLM.from_pretrained(self.config.mname).to(self.config.device)
        self.tokenizer = AutoTokenizer.from_pretrained(self.config.mname)

        self.chat_history = defaultdict(lambda: torch.as_tensor([], dtype=torch.int).to(self.config.device))

        self.retained_history_idx = 0   # only clip history after here
        self.history_clip_buffer  = 20  # clips the history a bit extra to allow for new response generation

    def set_persona(self, new_persona):
        """
        Set new persona, conforming to persona format in config
        """
        self.config.persona = new_persona

    def set_initial_history(self, user: str, convo_topic: str, return_history_idx: bool = False):
        """
        Set the history with the conversation topic and define retained_history_idx
        """
        persona_n_topic = self.config.persona + convo_topic + self.tokenizer.eos_token
        tokens = self.tokenizer.encode(persona_n_topic, return_tensors="pt").to(self.config.device)

        self.retained_history_idx = tokens.shape[-1]
        self.chat_history[user] = tokens

        if return_history_idx:
            return self.retained_history_idx, self.chat_history[user]

    def update_chat_history(self, user: str, new_response: str):
        """
        adds new response to chat_history
        """
        response_ids = self.tokenizer.encode(new_response + self.tokenizer.eos_token, return_tensors="pt").to(self.config.device)
        self.chat_history[user] = torch.cat([self.chat_history[user], response_ids], dim=-1).to(self.config.device)

        # clip history if necessary, and save new clipped history
        if self.chat_history[user].shape[-1] >= self.config.max_len_history:
            # history set to clipped history and input
            self.chat_history[user] = self.clip_history(self.chat_history[user])

    def clip_history(self, inputs):
        logging.info("Clipping %s history ...", self.config.name)

        history_to_keep = inputs[:, :self.retained_history_idx]
        excess = inputs.shape[-1] - self.config.max_len_history + self.history_clip_buffer
        clipped_history = inputs[:, self.retained_history_idx + excess:]
        # print("history to keep: ", self.tokenizer.decode(history_to_keep[:][0]))
        # print("clipped history: ", self.tokenizer.decode(clipped_history[:][0]))

        # keep only initial history and most recent history
        inputs = torch.cat([history_to_keep, clipped_history], dim=-1).to(self.config.device)
        return inputs

    def get_response(self, user: str, input_text: str = "", input_ids_extern = None):
        """
        Generates response based on history and input_text
        --> keep input_text empty if updated history with input first
        """
        logging.info("Getting %s response ...", self.config.name)

        if input_text:
            # encode input w/ EOS token
            _input_ids = self.tokenizer.encode(input_text + self.tokenizer.eos_token, return_tensors="pt").to(self.config.device)
            # Append tokens from new input to chat history for generating response
            input_ids = torch.cat([self.chat_history[user], _input_ids], dim=-1).to(self.config.device)
        elif input_ids_extern is not None:
            input_ids = torch.from_numpy(np.array(input_ids_extern))
        else:
            input_ids = self.chat_history[user]

        # model generates response while respecting max_length
        resp_w_history = self.model.generate(
            input_ids,
            do_sample   = self.config.do_sample,
            max_length  = self.config.max_len_history,
            top_p       = self.config.top_p,
            top_k       = self.config.top_k,
            temperature = self.config.temperature,
            repetition_penalty = self.config.repetition_penalty,
            # Passing EOL token id here from tokenizer to model.
            pad_token_id = self.tokenizer.eos_token_id
        )

        # decode new portion of chat history for output
        response = self.tokenizer.decode(resp_w_history[:, input_ids.shape[-1]:][0], skip_special_tokens=True)
        return response

    async def aget_response(self, user: str, input_text: str = "", input_ids_extern = None):
        return await asyncio.get_event_loop().run_in_executor(
            None, self.get_response, user, input_text, input_ids_extern
        )


if __name__ == "__main__":
    config = DialoGPTConfig()
    user_id = "1234567890"

    model = DialoGPT(config)
    print("Persona:", model.config.persona or None)

    def main():
        model.set_initial_history(user_id, "What's the point of existence?")
        for n in range(5):
            inp = input("you: ")
            resp = model.get_response(user_id, inp)
            resp = resp.strip(" \n\r\t")
            print(f"{model.config.name}:", resp)
            model.update_chat_history(user_id, inp)
            model.update_chat_history(user_id, resp)

    async def amain():
        async def bg():
            x = 0
            while True:
                print(f"\033[K\rSleep {x}s ...", end="")
                x += 1
                await asyncio.sleep(1)

        model.update_chat_history(user_id, "What's the point of existence?")
        for n in range(3):
            inp = input("you: ")

            t = asyncio.create_task(bg())
            resp = await model.aget_response(user_id, inp)
            t.cancel()

            resp = resp.strip(" \n\r\t")
            print(f"\033[K\r{model.config.name}:", resp)
            model.update_chat_history(user_id, inp)
            model.update_chat_history(user_id, resp)

    # main()
    asyncio.run(amain())
    print("history: ", model.tokenizer.decode(model.chat_history[user_id]["input_ids"][:][0], skip_special_tokens=True))
