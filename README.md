<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assets/morph_logo_rgb.png" alt="Morphysm" ></a>
  <br>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI .</h5>
  <br>
</h1>

<h1 align="center">

 <img src="https://img.shields.io/badge/Python-3.9-brightgreen" alt="python badge">

 <img src="https://img.shields.io/badge/docker-20.10-blue" alt="docker badge">

 <img src="https://img.shields.io/badge/AI%20Model-Blender-red" alt="AI badge">
</h1> 

# Table of Contents

<!--ts-->

- [Getting Started](#getting-started)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
  - [Download the project](#download-the-project)
  - [Non-docker](#non-docker)
  - [Docker](#docker)
  - [Run](#run)
- [Development](#development)
- [Test Our Model](#test_our_model)
- [Demo](#demo)
- [TroubelShoting](#troubelShoting)
- [Code Owners](#code-owners)
- [License](#license)
- [Contact](#contact)
<!--te-->

# 1. Getting Started

The Container for running the Dialogpt model is represented by this repository.

Dialogpt is a Large-Scale Generative Pre-training for Conversational Response Generation that is a GPT2 Model trained on 147M Reddit conversation-like exchanges.

DialoGPT was trained on conversational data with a causal language modeling (CLM) objective and is thus effective at response generation in open-domain dialogue systems. More information can be found [here](https://huggingface.co/transformers/model_doc/dialogpt.html).

# 2. Prerequisites

- `docker`
- `docker-compose`
- `python3.9`
- Requirements to run (minimal):

  - `8GB RAM`
  - `2 VCPU`

- Requirements to run (recommended):
  - `4 VCPU`

# 3. Installation

- ### Download the Project:
  To download our repo please write this command either with SHH:

    ```bash
    git clone git@gitlab.com:dicu.chat/container-model-dialogpt.git
    ```

  or with HTTPS:

  ```bash
  git clone https://gitlab.com/dicu.chat/container-model-dialogpt.git
  ```

- ### Non-Docker:

  ```bash
  sudo apt install nvidia-cuda-toolkit virtualenv python3.8-dev
  ```

- ### Docker:

  Installing nvidia docker runtime. Please follow this link[ here! ](https://github.com/NVIDIA/nvidia-container-runtime).

- ### Run:

  ```bash
  ./build.sh && ./run.sh
  ```

# 4. Development

Please follow these steps to help you run our project locally:

1. Install docker into your system. For ubuntu:

    ```bash
    sudo apt-get install docker.io
    ```

2. Activate the virtual environment if you already have installed virtualenv:

    ```bash
    virtualenv -p python3.9 .venv && source .venv/bin/activate
    ```

    or run this command if you have not install the virtualenv:

    ```bash
    python -m venv .venv && source .venv/bin/activate
    ```


3. Install dependencies:

    ```bash
    python -m pip install -r requirements.txt
    ```

4. Run the [server project](https://gitlab.com/dicu.chat/server) before you run this project, don't forget to update the `DIALOGPT_URL` variable in the [server repository .env file Line 46 ](https://gitlab.com/dicu.chat/server/-/blob/master/.env.example#L46) to reflect the local host and port, eg. `DIALOGPT_URL=http://0.0.0.0:8686`. also make sure to set the port in our [`server/__main__.py`](https://gitlab.com/dicu.chat/container-model-dialogpt/-/blob/master/server/__main__.py#L46) , line 46, so that there's no conflicts.<br> </br>


5. Run our project:

    ```bash
    python -m server
    ```

# 5. Test Our Model:

To put our model to the test and start a conversation with a dialogpt model's bot, Please run the [server repo](https://gitlab.com/dicu.chat/server) with this repo so they can start talking to each other. You must also run the [bridge telegram repo](https://gitlab.com/dicu.chat/bridge-telegram), but you must change the 'AI DEFAULT MODEL=dialogpt' in the server repo's[.env file on line 49](https://gitlab.com/dicu.chat/server/-/blob/master/.env.example#L49). Then, go to your Telegram bot channel and type /start then /ai to begin a conversation with our AI bot dialogpt model.

# 6. Demo: 

Will be updated soon...

# 7. TroubelShoting

If you have encountered any problems while running the code, please open a new issue in this repo and label it bug, and we will assist you in resolving it. :wink:


# 8. Code Owners

@morphysm/team :sunglasses:

# 9. License

Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/container-model-dialogpt/-/blob/master/LICENSE)

# 10. Contact

If you'd like to know more about us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".
